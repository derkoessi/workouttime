import 'package:hydrated_bloc/hydrated_bloc.dart';
import '../models/settings.dart';

class SettingsCubit extends HydratedCubit<Settings> {
  SettingsCubit() : super(const Settings('beep', 'ding', 'applause'));

  setSound(type, value) => (type == 'tick')
      ? emit(Settings(value, state.exEndSound, state.woEndSound))
      : (type == 'exEnd')
          ? emit(Settings(state.tickSound, value, state.woEndSound))
          : (type == 'woEnd')
              ? emit(Settings(state.tickSound, state.exEndSound, value))
              : null;

  @override
  Settings fromJson(Map<String, dynamic> json) => Settings.fromJson(json);

  @override
  Map<String, dynamic> toJson(Settings state) {
    if (state is Settings) {
      return state.toJson();
    } else {
      return null;
    }
  }
}
