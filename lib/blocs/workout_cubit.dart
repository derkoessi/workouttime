import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wakelock/wakelock.dart';
import '../models/exercise.dart';
import '../models/workout.dart';
import '../states/workout_state.dart';

class WorkoutCubit extends Cubit<WorkoutState> {
  WorkoutCubit() : super(const WorkoutInitial());

  Timer _timer;

  onTick(Timer timer) {
    if (state is WorkoutInProgress) {
      WorkoutInProgress wip = state as WorkoutInProgress;
      if (wip.elapsed < wip.workout.getTotal()) {
        emit(WorkoutInProgress(wip.workout, wip.elapsed + 1));
      } else {
        _timer.cancel();
        Wakelock.disable();
        emit(const WorkoutInitial());
      }
    }
  }

  startWorkout(Workout workout, [int index]) {
    Wakelock.enable();
    if (index != null) {
      emit(WorkoutInProgress(
          workout, workout.exercises.elementAt(index).startTime));
    } else {
      emit(WorkoutInProgress(workout, 0));
    }
    _timer = Timer.periodic(const Duration(seconds: 1), onTick);
  }

  skipPrelude() {
    Exercise currentExercise = state.workout.getCurrentExercise(state.elapsed);
    emit(WorkoutInProgress(
        state.workout, currentExercise.startTime + currentExercise.prelude));
  }

  skipExercise() {
    int currentIndex = state.workout.getCurrentExercise(state.elapsed).index;
    if (currentIndex < state.workout.exercises.length - 1) {
      int newElapsed =
          state.workout.exercises.elementAt(currentIndex + 1).startTime;
      emit(WorkoutInProgress(state.workout, newElapsed));
    }
  }

  pauseWorkout() => emit(WorkoutPaused(state.workout, state.elapsed));

  resumeWorkout() => emit(WorkoutInProgress(state.workout, state.elapsed));

  editWorkout(Workout workout, int index) =>
      emit(WorkoutEditing(workout, index, null));

  editExercise(int exIndex) => emit(
      WorkoutEditing(state.workout, (state as WorkoutEditing).index, exIndex));

  goHome() {
    Wakelock.disable();
    _timer?.cancel();
    emit(const WorkoutInitial());
  }
}
