import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:workouttime/blocs/settings_cubit.dart';
import '../models/workout.dart';
import '../models/exercise.dart';
import '../blocs/workout_cubit.dart';
import '../blocs/log_cubit.dart';
import '../components/blink_text.dart';
import '../states/workout_state.dart';
import '../helpers.dart';

class WorkoutPage extends StatelessWidget {
  const WorkoutPage({Key key}) : super(key: key);

  Map<String, dynamic> _getStats(Workout workout, int workoutElapsed) {
    int workoutTotal = workout.getTotal();
    Exercise exercise = workout.getCurrentExercise(workoutElapsed);
    int exerciseElapsed = workoutElapsed - exercise.startTime;
    int exerciseRemaining = exercise.prelude - exerciseElapsed;
    bool isPrelude = exerciseElapsed < exercise.prelude;
    int exerciseTotal = isPrelude ? exercise.prelude : exercise.duration;
    if (!isPrelude) {
      exerciseElapsed -= exercise.prelude;
      exerciseRemaining += exercise.duration;
    }
    return {
      "workoutTitle": workout.title,
      "workoutProgress": workoutElapsed / workoutTotal,
      "workoutElapsed": workoutElapsed,
      "totalExercises": workout.exercises.length,
      "currentExerciseIndex": exercise.index.toDouble(),
      "workoutRemaining": workoutTotal - workoutElapsed,
      "isPrelude": isPrelude,
      "currentExercise": exercise.title,
      "exerciseProgress": exerciseElapsed / exerciseTotal,
      "exerciseRemaining": exerciseRemaining,
      "nextExercise": (exercise.index < workout.exercises.length - 1)
          ? workout.exercises[exercise.index + 1].title
          : null
    };
  }

  @override
  Widget build(BuildContext context) {
    SettingsCubit settingsCubit = SettingsCubit();
    SoundPlayer.loadSounds(settingsCubit.state.tickSound,
        settingsCubit.state.exEndSound, settingsCubit.state.woEndSound);
    return BlocConsumer<WorkoutCubit, WorkoutState>(listener: (context, state) {
      if (state.workout != null) {
        final stats = _getStats(state.workout, state.elapsed);
        if (stats['exerciseRemaining'] == 0) {
          SoundPlayer.playWoEndSound();
          BlocProvider.of<LogCubit>(context).addLog(state.workout.title);
        } else if (stats['exerciseProgress'] == 0) {
          SoundPlayer.playExEndSound();
        } else if (stats['exerciseRemaining'] <= 3) {
          SoundPlayer.playTickSound();
        }
      }
    }, builder: (context, state) {
      final stats = _getStats(state.workout, state.elapsed);
      return WillPopScope(
          onWillPop: () => BlocProvider.of<WorkoutCubit>(context).goHome(),
          child: Scaffold(
              appBar: AppBar(
                  leading: BackButton(
                      onPressed: () =>
                          BlocProvider.of<WorkoutCubit>(context).goHome()),
                  title: Text(stats['workoutTitle'])),
              body: Container(
                  padding: const EdgeInsets.all(32),
                  child: Column(children: [
                    LinearProgressIndicator(
                        backgroundColor: Colors.blue[100],
                        minHeight: 10,
                        value: stats['workoutProgress']),
                    Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(formatTime(stats['workoutElapsed'], true),
                                  style: const TextStyle(fontSize: 16)),
                              DotsIndicator(
                                  dotsCount: stats['totalExercises'],
                                  position: stats['currentExerciseIndex']),
                              Text(
                                  '-' +
                                      formatTime(
                                          stats['workoutRemaining'], true),
                                  style: const TextStyle(fontSize: 16))
                            ])),
                    const Spacer(),
                    if (stats['isPrelude'])
                      InkWell(
                          onTap: () => BlocProvider.of<WorkoutCubit>(context)
                              .skipPrelude(),
                          child: Text(
                              '${AppLocalizations.of(context).getReadyFor}:',
                              style: const TextStyle(
                                  color: Colors.red, fontSize: 20))),
                    Text(stats['currentExercise'],
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold)),
                    const Spacer(),
                    InkWell(
                        onTap: () {
                          if (state is WorkoutInProgress) {
                            BlocProvider.of<WorkoutCubit>(context)
                                .pauseWorkout();
                          } else if (state is WorkoutPaused) {
                            BlocProvider.of<WorkoutCubit>(context)
                                .resumeWorkout();
                          }
                        },
                        child:
                            Stack(alignment: const Alignment(0, 0), children: [
                          Center(
                              child: SizedBox(
                                  height: 220,
                                  width: 220,
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          stats['isPrelude']
                                              ? Colors.red
                                              : Colors.blue),
                                      strokeWidth: 25,
                                      value: stats['exerciseProgress']))),
                          Center(
                            child: SizedBox(
                                height: 300,
                                width: 300,
                                child: Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Image.asset('stopwatch.png'))),
                          ),
                          SizedBox(
                              height: 200,
                              width: 200,
                              child: Center(
                                  child: (state is WorkoutPaused)
                                      ? BlinkText(formatTime(
                                          stats['exerciseRemaining'], false))
                                      : Text(
                                          formatTime(stats['exerciseRemaining'],
                                              false),
                                          style: const TextStyle(
                                              fontSize: 50,
                                              fontWeight: FontWeight.bold))))
                        ])),
                    const Spacer(),
                    InkWell(
                        onTap: () {
                          BlocProvider.of<WorkoutCubit>(context).skipExercise();
                        },
                        child: Text(
                            (stats['nextExercise'] != null)
                                ? "${AppLocalizations.of(context).nextExercise}: ${stats['nextExercise']}"
                                : AppLocalizations.of(context).almostDone,
                            style: const TextStyle(fontSize: 18)))
                  ]))));
    });
  }
}
